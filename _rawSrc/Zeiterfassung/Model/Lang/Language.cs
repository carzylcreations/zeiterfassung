﻿using System;
using System.Globalization;
using System.Threading;
using WpfLocalization;

namespace Zeiterfassung.Model.Lang
{
	public sealed class Language
	{
		public static readonly Language Instance = new Language();
		private Languages _activeLanguage;

		public Languages ActiveLanguage
		{
			get => _activeLanguage;
			set
			{
				_activeLanguage = value;
				Properties.Settings.Default.ActiveLanguage = value.ToString();
				Properties.Settings.Default.Save();

				switch (ActiveLanguage)
				{
					case Languages.Deutsch:
						ActiveCultureInfo = CultureInfo.GetCultureInfo("de");
						break;

					default:
						ActiveCultureInfo = CultureInfo.GetCultureInfo("en");
						break;
				}

				Thread.CurrentThread.CurrentCulture = ActiveCultureInfo;
				Thread.CurrentThread.CurrentUICulture = ActiveCultureInfo;

				LocalizationManager.UpdateValues();
				LanguageChanged?.Invoke(typeof(Language), EventArgs.Empty);
			}
		}

		public CultureInfo ActiveCultureInfo { get; private set; }

		/// <summary>
		/// Gets called if the <see cref="ActiveLanguage"/> changed.
		/// </summary>
		public event EventHandler LanguageChanged;

		private Language()
		{
			ActiveLanguage = LoadActiveLanguage();
		}

		private Languages LoadActiveLanguage()
		{
			if (Properties.Settings.Default.ActiveLanguage != null && Enum.TryParse(Properties.Settings.Default.ActiveLanguage, out Languages result))
			{
				return result;
			}
			else
			{
				if (Thread.CurrentThread.CurrentCulture.Name.StartsWith("de"))
					return Languages.Deutsch;
			}

			return Languages.English;
		}
	}
}