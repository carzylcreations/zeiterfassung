﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zeiterfassung.Data;
using Zeiterfassung.ViewModel;

namespace Zeiterfassung.Model.Helper
{
    public static class ListItemHelper
    {
        /// <summary>
        /// Updates the duration for the items in the list.
        /// </summary>
        public static void AdjustDuration(IList<ListItemVm> items)
        {
            if (items.Count == 0)
                return;

            if (items.Count > 1)
            {
                // Adjust duration except for first item
                for (int i = 1; i < items.Count; i++)
                {
                    // Duration = New starttime - old starttime
                    items[i].Duration = items[i - 1].Entry.StartTime - items[i].Entry.StartTime;
                }
            }

            // Adjust for first item (if from today)
            var first = items[0];
            if (first.Entry.StartTime.Date == DateTime.Today)
                first.Duration = DateTime.Now - first.Entry.StartTime;
        }

        /// <summary>
        /// Reloads the items, ordered by StartTime (Descending) with the correct durations.
        /// </summary>
        /// <param name="day"> The day to load the items for. May be null. </param>
        public static List<ListItemVm> ReloadItems(Day day)
        {
            if (day == null)
                return new List<ListItemVm>();

            using (var context = new DBContext())
            {
                var entries = context.Entries.Where(x => x.IDDay == day.ID).OrderByDescending(x => x.StartTime);

                var list = new List<ListItemVm>();
                foreach (var entry in entries)
                {
                    var task = context.Tasks.First(x => x.ID == entry.IDTask);
                    var project = context.Projects.First(x => x.ID == task.IDProject);

                    list.Add(new ListItemVm(project, task, entry));
                }

                AdjustDuration(list);

                return list;
            }
        }
    }
}