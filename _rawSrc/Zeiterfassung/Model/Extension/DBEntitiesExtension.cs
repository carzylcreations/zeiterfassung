﻿using System;
using System.Linq;
using Zeiterfassung.Data;

namespace Zeiterfassung.Model.Extension
{
    public static class DBEntitiesExtension
    {
        /// <summary>
        /// Returns an existing or creates and then returns a new day item.
        /// </summary>
        public static Day GetOrCreateDay(this DBContext context, DateTime date)
        {
            var day = context.Days.FirstOrDefault(x => x.Date == date.Date);
            if (day != null)
                return day;

            day = new Day() { Date = date.Date };
            context.Days.Add(day);
            return day;
        }

        /// <summary>
        /// Returns an existing or creates and then returns a new project item.
        /// </summary>
        public static Project GetOrCreateProject(this DBContext context, string title)
        {
            if (title.Length == 0)
                return Project.UndefinedProject;

            var project = context.Projects.FirstOrDefault(x => x.Title == title);
            if (project != null)
                return project;

            project = new Project() { Title = title };
            context.Projects.Add(project);
            return project;
        }

        /// <summary>
        /// Returns an existing or creates and then returns a new task item.
        /// </summary>
        public static Task GetOrCreateTask(this DBContext context, string title, Project project)
        {
            if (project == null)
                project = Project.UndefinedProject;

            var task = context.Tasks.FirstOrDefault(x => x.Title == title && x.IDProject == project.ID);

            if (task != null)
                return task;

            task = new Task() { Title = title, IDProject = project.ID };
            context.Tasks.Add(task);
            return task;
        }
    }
}