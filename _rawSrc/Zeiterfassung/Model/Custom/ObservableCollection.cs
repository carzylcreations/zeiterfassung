﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Zeiterfassung.Model.Custom
{
    public class ObservableCollection<T> : System.Collections.ObjectModel.ObservableCollection<T>
    {
        public ObservableCollection(IEnumerable<T> collection) : base(collection)
        {
        }

        public ObservableCollection(List<T> list) : base(list)
        {
        }

        public ObservableCollection()
        {
        }

        /// <summary>
        /// Adds a range of items to the collection.
        /// </summary>
        public void AddRange(IEnumerable<T> range)
        {
            foreach (var item in range)
            {
                Items.Add(item);
            }

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset)); // Reset, because replace works only for 1 item
        }

        /// <summary>
        /// Replaces all current items with the given range.
        /// </summary>
        public void ReplaceAll(IEnumerable<T> range)
        {
            var oldItems = Items.ToList();

            Items.Clear();

            foreach (var item in range)
            {
                Items.Add(item);
            }

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset)); // Reset, because replace works only for 1 item
        }
    }
}