﻿using System;
using System.IO;

namespace Zeiterfassung.Model
{
    internal static class StaticValues
    {
        public static readonly string AppDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TimeTracking");
        public static readonly string DefaultBackupFolderPath = Path.Combine(AppDataPath, "Backup");
    }
}