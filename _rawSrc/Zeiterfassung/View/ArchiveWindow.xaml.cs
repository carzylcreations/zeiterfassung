﻿using System.Windows;
using System.Windows.Input;
using Zeiterfassung.ViewModel;

namespace Zeiterfassung.View
{
    /// <summary>
    /// Interaktionslogik für ArchiveWindow.xaml
    /// </summary>
    public partial class ArchiveWindow : Window
    {
        public ArchiveWindow()
        {
            InitializeComponent();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }
    }
}