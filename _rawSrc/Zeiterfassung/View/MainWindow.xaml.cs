﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Zeiterfassung.ViewModel;

namespace Zeiterfassung.View
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainVm(this);
        }

        /// <summary>
        /// Is the input string a integer positive or negative number?
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private bool IsTextAllowed(string text)
        {
            return new Regex("^[0-9-:]+$").IsMatch(text); //regex that matches allowed text
        }

        private void MenuItem_Delete(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as MainVm;
            if (vm.DeleteTaskCommand.CanExecute())
                vm.DeleteTaskCommand.Execute(ListViewTasks.SelectedItem);
        }

        private void MenuItem_Reload(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as MainVm;
            if (vm.ReloadCommand.CanExecute())
                vm.ReloadCommand.Execute();
        }

        private void TextBoxAdjust_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TextBoxAdjust.Text))
            {
                TextBoxAdjust.Text = "0";
            }
        }

        private void TextBoxAdjust_PreviewGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBoxAdjust.SelectAll();
        }

        private void TextBoxAdjust_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //Cancel replace text if its not a integer
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void TextBoxProject_PreviewGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!TextBoxProject.IsFocused)
                TextBoxProject.SelectAll();
        }

        private void TextBoxTask_PreviewGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!TextBoxTask.IsFocused)
                TextBoxTask.SelectAll();
        }

        private void TextBoxTask_TextChanged(object sender, TextChangedEventArgs e)
        {
            BtnAdd.IsEnabled = !string.IsNullOrWhiteSpace(TextBoxTask.Text);
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (BtnAdd.IsEnabled)
                {
                    var vm = DataContext as MainVm;
                    if (vm.AddTaskCommand.CanExecute())
                        vm.AddTaskCommand.Execute();
                }
            }
            else if (e.Key == Key.Escape)
            {
                Close();
            }
        }
    }
}