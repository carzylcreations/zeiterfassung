﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Zeiterfassung.ViewModel;
using static Zeiterfassung.ViewModel.SummaryVm;

namespace Zeiterfassung.View
{
    /// <summary>
    /// Interaktionslogik für SummaryWindow.xaml
    /// </summary>
    public partial class SummaryWindow : Window
    {
        public SummaryVm Vm => DataContext as SummaryVm;

        public SummaryWindow()
        {
            InitializeComponent();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    Close();
                    break;

                case Key.C:
                    if (e.KeyboardDevice.Modifiers.HasFlag(ModifierKeys.Control))
                    {
                        bool copyOnlyTitles = e.KeyboardDevice.Modifiers.HasFlag(ModifierKeys.Alt);
                        CopySelected(copyOnlyTitles);
                    }
                    break;
            }
        }

        /// <param name="copyOnlyTitles"> Sollen nur die Aufgabentitel kopiert werden? </param>
        private void CopySelected(bool copyOnlyTitles = false)
        {
            try
            {
                var selected = ListViewItems.SelectedItems.Cast<SummaryItem>().ToList();
                if (selected.Count == 0)
                    return;

                var builder = new StringBuilder();

                switch (Vm.ActiveMode)
                {
                    case ValueModes.Project:
                        foreach (var item in selected)
                        {
                            if (copyOnlyTitles)
                                builder.AppendLine(item.Project.ToString());
                            else
                                builder.AppendLine($"{item.Project}\t{item.TotalDurationString}");
                        }
                        break;

                    case ValueModes.Task:
                        foreach (var item in selected)
                        {
                            if (copyOnlyTitles)
                                builder.AppendLine(item.Task.ToString());
                            else
                                builder.AppendLine($"{item.Project}\t{item.Task}\t{item.TotalDurationString}");
                        }
                        break;
                }

                Clipboard.SetDataObject(builder.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Kopieren fehlgeschlagen:\n" + ex);
            }
        }
    }
}