﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace Zeiterfassung.View
{
    /// <summary>
    /// Interaktionslogik für InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        private Regex _textRegex;

        /// <summary>
        /// The title in large blue letters at the top of the page.
        /// </summary>
        public string Caption
        {
            get => lblTitle.Content.ToString();
            set
            {
                lblTitle.Content = value;
                if (string.IsNullOrWhiteSpace(value))
                    lblTitle.Visibility = Visibility.Collapsed;
                else
                    lblTitle.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// The description text above the input field.
        /// </summary>
        public string Description
        {
            get => textBlockDescription.Text;
            set
            {
                textBlockDescription.Text = value;
                if (string.IsNullOrWhiteSpace(value))
                    textBlockDescription.Visibility = Visibility.Collapsed;
                else
                    textBlockDescription.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// The content of the input field.
        /// </summary>
        public string Input
        {
            get => textBoxInput.Text;
            set { textBoxInput.Text = value; }
        }

        /// <summary>
        /// A regex to check every new charakter against. If not fullfilled, the character won't be added. If null there is no check.
        /// </summary>
        public Regex InputRegex { get; set; }

        /// <summary>
        /// A regex to check the whole input text against. If not fullfilled the OK-Button won't be active. If null there is no check.
        /// </summary>
        public Regex TextRegex
        {
            get { return _textRegex; }
            set
            {
                _textRegex = value;
                CheckBtnOkEnabled();
            }
        }

        public InputDialog()
        {
            InitializeComponent();

            Caption = "";
            Description = "";
            Input = "";
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void CheckBtnOkEnabled()
        {
            btnOk.IsEnabled = TextRegex?.IsMatch(textBoxInput.Text) != false;
        }

        private void textBoxInput_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (InputRegex?.IsMatch(e.Text) == false)
            {
                e.Handled = true;
            }
        }

        private void textBoxInput_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CheckBtnOkEnabled();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && btnOk.IsEnabled)
            {
                DialogResult = true;
                Close();
            }
            else if (e.Key == Key.Escape)
            {
                DialogResult = false;
                Close();
            }
        }
    }
}