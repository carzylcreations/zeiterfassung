﻿using System.Windows;
using Zeiterfassung.ViewModel;

namespace Zeiterfassung.View
{
    /// <summary>
    /// Interaktionslogik für SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();
            DataContext = new SettingsVm(this);
        }

        /// <summary>
        /// Were some changes made that require a reload of the main windows items?
        /// </summary>
        public bool IsDirty => (DataContext as SettingsVm)?.IsDirty ?? false;
    }
}