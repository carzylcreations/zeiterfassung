﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace Zeiterfassung
{
    internal static class EntryPoint
    {
        private const string _unique = "net.weidmann.timetracking";

        /// <summary>
        /// Window gets restored to previous size and position.
        /// </summary>
        private const int SW_RESTORE = 9;

        [STAThread]
        public static void Main()
        {
            using (var mutex = new Mutex(true, _unique, out bool createdNew))
            {
                if (createdNew)
                {
                    var app = new App();
                    app.InitializeComponent();
                    app.Run();
                }
                else
                {
                    var current = Process.GetCurrentProcess();
                    foreach (var process in Process.GetProcessesByName(current.ProcessName))
                    {
                        if (process.Id != current.Id)
                        {
                            ShowWindow(process.MainWindowHandle, SW_RESTORE);
                            BringWindowToTop(process.MainWindowHandle);
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Brings the specified window to the top of the Z order. If the window is a top-level window, it is activated. If the window is a child window, the top-level parent window associated with the child window is activated.
        /// </summary>
        /// <param name="hWnd"> A handle to the window to bring to the top of the Z order. </param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        private static extern bool BringWindowToTop(IntPtr hWnd);

        /// <summary>
        /// Sets the specified window's show state.
        /// </summary>
        /// <param name="hWnd"> A handle to the window. </param>
        /// <param name="nCmdShow"> Controls how the window is to be shown. </param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
    }
}