using System;
using System.Diagnostics;
using SQLite;

namespace Zeiterfassung.Data
{
    [DebuggerDisplay("ID: {ID}, Title: {Title}")]
    public class Project : IComparable<Project>
    {
        private static Project _undefinedProject;

        /// <summary>
        /// A project whose title is <see cref="string.Empty"/>.
        /// </summary>
        public static Project UndefinedProject
        {
            get
            {
                if (_undefinedProject != null)
                    return _undefinedProject;

                using (var context = new DBContext())
                {
                    var project = context.Projects.FirstOrDefault(x => x.Title == string.Empty);
                    if (project == null)
                    {
                        project = new Project { Title = string.Empty };
                        context.Projects.Add(project);
                    }

                    return _undefinedProject = project;
                }
            }
        }

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string Title { get; set; }

        public static bool operator !=(Project a, Project b)
        {
            return !(a == b);
        }

        public static bool operator ==(Project a, Project b)
        {
            if (ReferenceEquals(a, null)) // Do not use "==" as this will lead to SOE
                return ReferenceEquals(b, null);

            return a.Equals(b);
        }

        public int CompareTo(Project other)
        {
            if (Title == null && other.Title == null)
                return 0;
            return string.Compare(Title, other.Title);
        }

        public override bool Equals(object obj)
        {
            if (obj is Project other)
                return CompareTo(other) == 0;

            return false;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;

                if (Title != null)
                    hash = (hash * 59) + Title.GetHashCode();

                return hash;
            }
        }

        public override string ToString()
        {
            return Title;
        }
    }
}