using System;
using SQLite;

namespace Zeiterfassung.Data
{
    public class Task : IComparable<Task>
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public int? IDProject { get; set; }

        public string Title { get; set; }

        public static bool operator !=(Task a, Task b)
        {
            return !(a == b);
        }

        public static bool operator ==(Task a, Task b)
        {
            if (ReferenceEquals(a, null)) // Do not use "==" as this will lead to SOE
                return ReferenceEquals(b, null);

            return a.Equals(b);
        }

        public int CompareTo(Task other)
        {
            if (Title == null && other.Title == null)
                return 0;
            return string.Compare(Title, other.Title);
        }

        public override string ToString()
        {
            return Title;
        }

        public override bool Equals(object obj)
        {
            if (obj is Task other)
                return CompareTo(other) == 0;

            return false;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;

                if (Title != null)
                    hash = (hash * 59) + Title.GetHashCode();

                return hash;
            }
        }

    }
}