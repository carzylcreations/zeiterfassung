﻿using SQLite;

namespace Zeiterfassung.Data
{
    public static class TableQueryExtension
    {
        public static int Add<T>(this TableQuery<T> table, T value)
        {
            return table.Connection.Insert(value);
        }

        public static int Remove<T>(this TableQuery<T> table, T value)
        {
            return table.Connection.Delete(value);
        }

        public static int Update<T>(this TableQuery<T> table, T value)
        {
            return table.Connection.Update(value);
        }
    }
}