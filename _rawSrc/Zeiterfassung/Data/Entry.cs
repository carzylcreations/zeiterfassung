using System;
using SQLite;

namespace Zeiterfassung.Data
{
    public class Entry : IComparable<Entry>
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public int IDDay { get; set; }

        public int IDTask { get; set; }

        public DateTime StartTime { get; set; }

        public static bool operator !=(Entry a, Entry b)
        {
            return !(a == b);
        }

        public static bool operator ==(Entry a, Entry b)
        {
            if (ReferenceEquals(a, null)) // Do not use "==" as this will lead to SOE
                return ReferenceEquals(b, null);

            return a.Equals(b);
        }

        public int CompareTo(Entry other)
        {
            return StartTime.CompareTo(other?.StartTime);
        }

        public override bool Equals(object obj)
        {
            if (obj is Entry other)
                return CompareTo(other) == 0;

            return false;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;

                hash = (hash * 59) + StartTime.GetHashCode();

                return hash;
            }
        }

        public override string ToString()
        {
            return StartTime.ToString();
        }
    }
}