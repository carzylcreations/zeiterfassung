namespace Zeiterfassung.Data
{
	using System;
	using SQLite;
	using Zeiterfassung.Model.Lang;

	public class Day : IComparable<Day>
	{
		public DateTime? ArchivedDate { get; set; }
		public DateTime Date { get; set; }

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public bool IsArchived { get; set; }

		public int CompareTo(Day other)
		{
			return Date.CompareTo(other.Date);
		}

		public override string ToString()
		{
			return Date.ToShortDateString() + " (" + Date.ToString("dddd", Language.Instance.ActiveCultureInfo) + ")";
		}
	}
}