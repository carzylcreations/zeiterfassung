﻿using System;
using System.IO;
using SQLite;
using Zeiterfassung.Model;

namespace Zeiterfassung.Data
{
    public class DBContext : IDisposable
    {
        public static readonly string DefaultDatabaseFolderPath = Path.Combine(StaticValues.AppDataPath, "Database");
        public static readonly string DefaultDatabasePath = Path.Combine(DefaultDatabaseFolderPath, "TimeTracking.sqlite3");

        public SQLiteConnection Database { get; }

        /// <summary>
        /// The path to the database, managed by this connection.
        /// </summary>
        public string DatabasePath { get; }

        public virtual TableQuery<Day> Days => Database.Table<Day>();
        public virtual TableQuery<Entry> Entries => Database.Table<Entry>();
        public virtual TableQuery<Project> Projects => Database.Table<Project>();
        public virtual TableQuery<Task> Tasks => Database.Table<Task>();

        public DBContext() : this(DefaultDatabasePath)
        {
        }

        /// <param name="databasePath"> Ein Pfad zu einer eigenen Datenbank. </param>
        public DBContext(string databasePath)
        {
            if (databasePath == null)
                throw new ArgumentNullException(nameof(databasePath));
            if (string.IsNullOrWhiteSpace(databasePath))
                throw new ArgumentException("Datenbankpfad darf nicht leer sein.", nameof(databasePath));

            DatabasePath = databasePath;

            bool init = false;
            if (!File.Exists(databasePath))
                init = true;

            if (!Directory.Exists(DefaultDatabaseFolderPath))
                Directory.CreateDirectory(DefaultDatabaseFolderPath);
            Database = new SQLiteConnection(databasePath);

            if (init)
            {
                // Init Tabellen
                Database.CreateTable<Day>();
                Database.CreateTable<Entry>();
                Database.CreateTable<Task>();
                Database.CreateTable<Project>();
            }
        }

        public virtual void Dispose()
        {
            Database.Dispose();
        }
    }
}