﻿using System;
using Zeiterfassung.Data;

namespace Zeiterfassung.ViewModel
{
    public class ArchiveItemVm : ViewModelBase
    {
        /// <summary>
        /// Gets called if the day was restored.
        /// </summary>
        public Action<Day> Restored;

        private readonly Day _day;

        public string Content { get; }

        #region Commands

        public RelayCommand RestoreCommand { get; }

        #endregion Commands

        public ArchiveItemVm(Day day)
        {
            _day = day;
            Content = _day.Date.ToString("yyyy-MM-dd (dddd)");

            RestoreCommand = new RelayCommand(ExecuteRestore);
        }

        #region Command Methods

        private void ExecuteRestore()
        {
            using (var context = new DBContext())
            {
                _day.IsArchived = false;
                context.Days.Update(_day);
                Restored(_day);
            }
        }

        #endregion Command Methods
    }
}