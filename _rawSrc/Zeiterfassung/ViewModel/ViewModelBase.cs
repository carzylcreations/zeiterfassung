﻿using System.ComponentModel;
using System.Linq;

namespace Zeiterfassung.ViewModel
{
	/// <summary>
	/// Base class for all viewmodels.
	/// </summary>
	public abstract class ViewModelBase : INotifyPropertyChanged
	{
		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary> Notify, that the value of a property has changed. </summary>
		/// <param name="propertyName"> Designate the name of the property. </param>
		/// <param name="asynchron"> Raise the PropertyChangedEvent asynchron. </param>
		/// <param name="furtherPropertyNames"> Designate names of other properties. </param>
		[System.Diagnostics.DebuggerStepThrough]
		protected virtual void NotifyPropertyChanged(string propertyName, bool asynchron = false, params string[] furtherPropertyNames)
		{
			if (PropertyChanged != null)
			{
				NotifyPropertyChanged(new PropertyChangedEventArgs(propertyName), asynchron);
				foreach (string furtherPropertyName in furtherPropertyNames)
				{
					NotifyPropertyChanged(new PropertyChangedEventArgs(furtherPropertyName), asynchron);
				}
			}
		}

		/// <summary>
		/// Check for unequal values => then set the value and raise the <see cref="PropertyChanged"/>-Event
		/// for the name of the calling Property (get automatic via StackTrace).
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="oldValue"> Use "ref _myPrivateBackingField", maybe change this backing-field of the property. </param>
		/// <param name="newValue"> Maybe the new value of the property. </param>
		/// <param name="propertyName"> Designate the name of the property. </param>
		/// <param name="asynchron"> Raise the PropertyChangedEvent asynchron. </param>
		/// <param name="furtherPropertyNames"> Designate names of other properties. </param>
		/// <exception cref="System.ArgumentException"> "Caller is not a property" (stacktrace not contains a property on first level) </exception>
		protected bool SetAndNotifyIfChanged<T>(ref T oldValue, T newValue, string propertyName, bool asynchron = false, params string[] furtherPropertyNames)
		{
			if (!Equals(oldValue, newValue))
			{
				oldValue = newValue;
				NotifyPropertyChanged(propertyName, asynchron, furtherPropertyNames);
				return true;
			}
			return false;
		}

		/// <summary> Check for unequal references => then set the value and raise the <see cref="PropertyChanged"/>-Event
		/// for the name of the calling Property (get automatic via StackTrace).
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="oldValue"> Use "ref _myPrivateBackingField", maybe change this backing-field of the property. </param>
		/// <param name="newValue"> Maybe the new value of the property. </param>
		/// <param name="propertyName"> Designate the name of the property. </param>
		/// <param name="asynchron"> Raise the PropertyChangedEvent asynchron. </param>
		/// <param name="furtherPropertyNames"> Designate names of other properties. </param>
		/// <exception cref="System.ArgumentException"> "Caller is not a property" (stacktrace not contains a property on first level) </exception>
		protected bool SetAndNotifyIfRefChanged<T>(ref T oldValue, T newValue, string propertyName, bool asynchron = false, params string[] furtherPropertyNames)
		{
			if (!ReferenceEquals(oldValue, newValue))
			{
				oldValue = newValue;
				NotifyPropertyChanged(propertyName, asynchron, furtherPropertyNames);
				return true;
			}
			return false;
		}

		/// <summary> Notify, that the value of a property has changed. </summary>
		/// <param name="e"> EventArgs, which contains PropertyName information. </param>
		/// <param name="asynchron"> Raise the PropertyChangedEvent asynchron. </param>
		[System.Diagnostics.DebuggerStepThrough]
		private void NotifyPropertyChanged(PropertyChangedEventArgs e, bool asynchron = false)
		{
			if (PropertyChanged != null)
			{
				if (asynchron)
				{
					System.Delegate[] delegates = PropertyChanged.GetInvocationList();
					foreach (var propertyChangedEvent in delegates.Cast<PropertyChangedEventHandler>())
					{
						propertyChangedEvent.BeginInvoke(this, e, null, null);
					}
				}
				else
				{
					PropertyChanged(this, e);
				}
			}
		}
	}
}