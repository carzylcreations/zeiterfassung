﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using Zeiterfassung.Data;
using Zeiterfassung.Model.Custom;
using Zeiterfassung.Model.Helper;
using Zeiterfassung.Model.Lang;
using Zeiterfassung.Properties;
using Zeiterfassung.View;
using static Zeiterfassung.Model.Extension.DBEntitiesExtension;

namespace Zeiterfassung.ViewModel
{
    public class SummaryVm : ViewModelBase
    {
        private readonly SummaryWindow _parent;
        private ValueModes _activeMode;
        private Visibility _greenFlagVisibility;
        private Visibility _redFlagVisibility;

        public ObservableCollection<Day> Days { get; } = new ObservableCollection<Day>();

        public Visibility GreenFlagVisibility
        {
            get { return _greenFlagVisibility; }
            set
            {
                _greenFlagVisibility = value;
                NotifyPropertyChanged(nameof(GreenFlagVisibility));
            }
        }

        /// <summary>
        /// The items to display.
        /// </summary>
        public ObservableCollection<SummaryItem> Items { get; } = new ObservableCollection<SummaryItem>();

        public Visibility ProjectButtonVisibility { get; private set; }

        public Visibility RedFlagVisibility
        {
            get { return _redFlagVisibility; }
            set
            {
                _redFlagVisibility = value;
                NotifyPropertyChanged(nameof(RedFlagVisibility));
            }
        }

        public Day SelectedDay { get; set; }
        public Visibility TaskButtonVisibility => ProjectButtonVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        public int TaskWidth => ActiveMode == ValueModes.Task ? 250 : 0;
        public string ValueName => ActiveMode == ValueModes.Project ? Resources.Project : Resources.Task;

        public ValueModes ActiveMode
        {
            get { return _activeMode; }
            private set
            {
                _activeMode = value;
                if (ActiveMode == ValueModes.Project)
                {
                    ProjectButtonVisibility = Visibility.Visible;
                }
                else
                {
                    ProjectButtonVisibility = Visibility.Collapsed;
                }

                NotifyPropertyChanged(nameof(ActiveMode), false, nameof(ValueName), nameof(TaskWidth), nameof(ProjectButtonVisibility), nameof(TaskButtonVisibility));
            }
        }

        /// <summary>
        /// The unsummarized items. Is used for switching fast between the <see cref="ValueModes"/>.
        /// </summary>
        private List<ListItemVm> CurrentItems { get; set; } = new List<ListItemVm>();

        /// <summary>
        /// Gets called if a entry was added or changed for the currently selected day.
        /// </summary>
        public event EventHandler<Day> DayEntryChanged;

        public event EventHandler DayArchived;

        #region Commands

        public RelayCommand ArchiveDayCommand { get; }
        public RelayCommand DaySelectionChangedCommand { get; }
        public RelayCommand EndOfWorkCommand { get; }
        public RelayCommand ToggleModeCommand { get; }

        #endregion Commands

        public SummaryVm(SummaryWindow parent, MainVm mainVm)
        {
            _parent = parent;

            ArchiveDayCommand = new RelayCommand(ExecuteArchiveDay);
            EndOfWorkCommand = new RelayCommand(ExecuteEndOfWork);
            DaySelectionChangedCommand = new RelayCommand(ExecuteDaySelectionChanged);
            ToggleModeCommand = new RelayCommand(ExecuteToggleMode);

            mainVm.Items.CollectionChanged += MainVm_Items_CollectionChanged;
            mainVm.DaysChanged += MainVm_DaysChanged;
            _parent.Closed += _parent_Closed;
            Language.Instance.LanguageChanged += Instance_LanguageChanged;

            // Init
            ReloadDays();

            // Initial change mode (to the same) to set the related localizations
            ActiveMode = ValueModes.Project;

            // Display first day
            var days = Days;
            if (days.Count > 0)
            {
                SelectedDay = days.First();
                NotifyPropertyChanged(nameof(SelectedDay));
                ReloadItems();
            }
        }

        private void AddDummyItem(IList<ListItemVm> items)
        {
            items.Add(new ListItemVm(Project.UndefinedProject, new Task(), new Entry()));
        }

        private bool ContainsEndOfTime(List<ListItemVm> items)
        {
            return items.Any(x => x.Task.Title == Resources.EndOfWork);
        }

        private void ReloadDays()
        {
            var selectedDay = SelectedDay;

            using (var context = new DBContext())
            {
                Days.ReplaceAll(context.Days.Where(x => !x.IsArchived).OrderByDescending(x => x.Date));
            }

            if (selectedDay != null)
                SelectedDay = Days.FirstOrDefault(x => x.Date == selectedDay.Date) ?? Days.FirstOrDefault();

            NotifyPropertyChanged(nameof(Days), false, nameof(SelectedDay));
        }

        /// <summary>
        /// Summarizes the given items for displaying.
        /// </summary>
        private ObservableCollection<SummaryItem> SummarizeItems(List<ListItemVm> items)
        {
            List<SummaryItem> list;

            // Flag visibility
            if (ContainsEndOfTime(items))
            {
                RedFlagVisibility = Visibility.Collapsed;
                GreenFlagVisibility = Visibility.Visible;
            }
            else
            {
                RedFlagVisibility = Visibility.Visible;
                GreenFlagVisibility = Visibility.Collapsed;
            }

            // Summarize
            if (ActiveMode == ValueModes.Project)
            {
                list = SummarizeProjects(items);
                list = list.Where(x => x.TotalDuration != TimeSpan.Zero).OrderByDescending(x => x.TotalDuration).ToList();
            }
            else if (ActiveMode == ValueModes.Task)
            {
                list = SummarizeTasks(items);
                list = OrderTasks(list);
            }
            else
            {
                return new ObservableCollection<SummaryItem>();
            }

            return new ObservableCollection<SummaryItem>(list.Where(x => x.TotalDuration != TimeSpan.Zero));
        }

        private List<SummaryItem> OrderTasks(List<SummaryItem> list)
        {
            var groups = new Dictionary<string, List<SummaryItem>>();
            foreach (var item in list)
            {
                if (groups.TryGetValue(item.Project.Title, out List<SummaryItem> group))
                {
                    group.Add(item);
                }
                else
                {
                    groups.Add(item.Project.Title, new List<SummaryItem> { item });
                }
            }

            var orderedGroups = groups.OrderBy(group => group.Key).Select(group => group.Value.OrderByDescending(item => item.TotalDuration));
            return orderedGroups.SelectMany(x => x).ToList();
        }

        private List<SummaryItem> SummarizeProjects(List<ListItemVm> items)
        {
            var dict = new Dictionary<Project, TimeSpan>();
            foreach (var item in items)
            {
                if (item.Duration == default(TimeSpan))
                    continue;

                if (dict.TryGetValue(item.Project, out TimeSpan value))
                {
                    dict[item.Project] = value + item.Duration;
                }
                else
                {
                    dict.Add(item.Project, item.Duration);
                }
            }

            var list = new List<SummaryItem>();
            foreach (var key in dict.Keys)
            {
                if (key.ID > 0)
                    list.Add(new SummaryItem(key, null, dict[key]));
                else
                    list.Add(new SummaryItem(null, null, dict[key]));
            }

            return list;
        }

        private List<SummaryItem> SummarizeTasks(List<ListItemVm> items)
        {
            var dict = new Dictionary<string, List<ListItemVm>>();

            foreach (var item in items)
            {
                string key = item.Project.Title + item.Task.Title;
                if (dict.TryGetValue(key, out List<ListItemVm> listItems))
                {
                    listItems.Add(item);
                }
                else
                {
                    dict.Add(key, new List<ListItemVm>() { item });
                }
            }

            var list = new List<SummaryItem>();

            foreach (var key in dict.Keys)
            {
                var totalDuration = TimeSpan.Zero;
                var listItems = dict[key];
                foreach (var listItem in listItems)
                {
                    totalDuration += listItem.Duration;
                }

                var firstItem = listItems[0];
                list.Add(new SummaryItem(firstItem.Project, firstItem.Task, totalDuration));
            }

            return list;
        }

        #region Command Methods

        /// <summary>
        /// Archives the selected day. It is no longer available over the main interface.
        /// </summary>
        /// <param name="obj"></param>
        private void ExecuteArchiveDay(object obj)
        {
            if (obj is Day item)
            {
                using (var context = new DBContext())
                {
                    var day = context.Days.First(x => x.Date == item.Date);

                    day.IsArchived = true;
                    day.ArchivedDate = DateTime.Now;

                    context.Days.Update(day);
                }

                NotifyPropertyChanged(nameof(Days));
                _parent.ComboBoxDays.Focus();

                ReloadDays();
                ReloadItems();

                DayArchived?.Invoke(this, EventArgs.Empty);
            }
        }

        private void ExecuteDaySelectionChanged()
        {
            ReloadItems();
        }

        private void ExecuteEndOfWork()
        {
            if (SelectedDay == null)
                return;

            var dialog = new InputDialog
            {
                Title = Resources.AddEndOfWork,
                Description = Resources.EnterEndOfWork,
                InputRegex = new Regex("[0-9:]"),
                TextRegex = new Regex("^[0-9]?[0-9](:[0-5][0-9])?$")
            };

            if (dialog.ShowDialog() != true)
                return;

            bool success = DateTime.TryParse(dialog.Input, out DateTime time);
            success = int.TryParse(dialog.Input, out int hour) || success;

            if (!success)
                return;

            if (time == DateTime.MinValue)
                time = SelectedDay.Date.AddHours(hour);

            using (var context = new DBContext())
            {
                var task = context.GetOrCreateTask(Resources.EndOfWork, null);
                var eowEntry = context.Entries.FirstOrDefault(x => x.IDDay == SelectedDay.ID && x.IDTask == task.ID);

                if (eowEntry != null)
                {
                    eowEntry.StartTime = time;

                    context.Entries.Update(eowEntry);
                }
                else
                {
                    eowEntry = new Entry { IDDay = SelectedDay.ID, IDTask = task.ID, StartTime = time };
                    context.Entries.Add(eowEntry);
                }
            }

            ReloadItems();
            DayEntryChanged?.Invoke(this, SelectedDay);
        }

        private void ExecuteToggleMode()
        {
            if (ActiveMode == ValueModes.Project)
            {
                ActiveMode = ValueModes.Task;
            }
            else
            {
                ActiveMode = ValueModes.Project;
            }

            Items.ReplaceAll(SummarizeItems(CurrentItems));
        }

        private void ReloadItems()
        {
            var items = ListItemHelper.ReloadItems(SelectedDay);

            // Add dummy item if the selected day is today => The current worktime can also get summarized
            if (SelectedDay?.Date == DateTime.Today)
                AddDummyItem(items);

            CurrentItems = items;
            Items.ReplaceAll(SummarizeItems(items));
        }

        #endregion Command Methods

        #region Handler

        private void Instance_LanguageChanged(object sender, EventArgs e)
        {
            ReloadDays();
        }

        private void _parent_Closed(object sender, EventArgs e)
        {
            Language.Instance.LanguageChanged -= Instance_LanguageChanged;
        }

        private void MainVm_DaysChanged(object sender, EventArgs e)
        {
            NotifyPropertyChanged(nameof(Days));
            ReloadItems();
        }

        private void MainVm_Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ReloadItems();
        }

        #endregion Handler

        public enum ValueModes
        {
            Project,
            Task
        }

        public class SummaryItem
        {
            public Project Project { get; }
            public Task Task { get; }
            public TimeSpan TotalDuration { get; }
            public string TotalDurationString { get { return TotalDuration.ToString("hh':'mm"); } }

            public SummaryItem(Project project, Task task, TimeSpan totalDuration)
            {
                Project = project;
                TotalDuration = totalDuration;
                Task = task;
            }
        }
    }
}