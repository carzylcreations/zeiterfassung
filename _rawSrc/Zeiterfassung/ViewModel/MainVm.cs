﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Zeiterfassung.Data;
using Zeiterfassung.Model.Custom;
using Zeiterfassung.Model.Helper;
using Zeiterfassung.Model.Lang;
using Zeiterfassung.Properties;
using Zeiterfassung.View;
using static Zeiterfassung.Model.Extension.DBEntitiesExtension;

namespace Zeiterfassung.ViewModel
{
    public class MainVm : ViewModelBase
    {
        private static readonly Brush _redundantBgColor = (Brush)new BrushConverter().ConvertFromString("#33FF0000");
        private readonly MainWindow _parent;

        public TimeSpan CurrentRuntime
        {
            get
            {
                // Don't show runtime from other days than today
                if (SelectedDay? // Roundwork because SelectedDay is null at the first start
                    .Date != DateTime.Today)
                {
                    return TimeSpan.Zero;
                }

                if (Items.Count > 0)
                    return DateTime.Now - Items.First().Entry.StartTime;

                return TimeSpan.Zero;
            }
        }

        /// <summary>
        /// The current runtime in a hh:mm format.
        /// </summary>
        public string CurrentRuntimeString
        {
            get
            {
                var total = CurrentRuntime;
                if (total == TimeSpan.Zero)
                    return "/";

                if (total.TotalSeconds > 0)
                    return total.ToString("hh':'mm");
                else
                    return "-" + total.ToString("hh':'mm");
            }
        }

        public ObservableCollection<Day> Days { get; } = new ObservableCollection<Day>();

        public Visibility GreenFlagVisibility => ContainsEndOfTime(Items) ? Visibility.Visible : Visibility.Collapsed;

        /// <summary>
        /// The items to show in the list. The first item is the newest, the last the oldest.
        /// </summary>
        public ObservableCollection<ListItemVm> Items { get; } = new ObservableCollection<ListItemVm>();

        public Visibility RedFlagVisibility => ContainsEndOfTime(Items) ? Visibility.Collapsed : Visibility.Visible;

        /// <summary>
        /// The <see cref="Items"/>, but in reverse order.
        /// </summary>
        public ObservableCollection<ListItemVm> ReverseItems => new ObservableCollection<ListItemVm>(Items.Reverse());

        /// <summary>
        /// The currently selected day to edit.
        /// </summary>
        public Day SelectedDay { get; set; }

        public TimeSpan TotalWorkTime
        {
            get
            {
                if (SelectedDay? // Roundwork because SelectedDay is null at the first start
                    .Date != DateTime.Today)
                {
                    return TimeSpan.Zero;
                }

                if (Items.Count > 0)
                    return DateTime.Now - Items.Last().Entry.StartTime;

                return TimeSpan.Zero;
            }
        }

        public string TotalWorkTimeString
        {
            get
            {
                var total = TotalWorkTime;
                if (total == TimeSpan.Zero)
                    return "/";

                return total.ToString("hh':'mm");
            }
        }

        public string VersionString => Assembly.GetEntryAssembly().GetName().Version.ToString(3);

        /// <summary>
        /// Gets called if the days were changed and should get reloaded.
        /// </summary>
        public event EventHandler DaysChanged;

        /// <summary>
        /// Gets called if the items were changed and should get reloaded.
        /// </summary>
        public event EventHandler ItemsChanged;

        #region Commands

        public RelayCommand AddTaskCommand { get; }
        public RelayCommand ArchiveDayCommand { get; }
        public RelayCommand ChangeLanguageCommand { get; }
        public RelayCommand DaySelectionChangedCommand { get; }
        public RelayCommand DeleteTaskCommand { get; }
        public RelayCommand EndOfWorkCommand { get; }
        public RelayCommand LunchbreakCommand { get; }
        public RelayCommand OpenArchiveCommand { get; }
        public RelayCommand OpenInfoCommand { get; }
        public RelayCommand OpenSettingsCommand { get; }
        public RelayCommand OpenSummaryCommand { get; }
        public RelayCommand ReloadCommand { get; }
        public RelayCommand SelectionChangedCommand { get; }

        #endregion Commands

        public MainVm(MainWindow parent)
        {
            _parent = parent;

            AddTaskCommand = new RelayCommand(ExecuteAddTask);
            ChangeLanguageCommand = new RelayCommand(ExecuteChangeLanguage);
            ArchiveDayCommand = new RelayCommand(ExecuteArchiveDay);
            DeleteTaskCommand = new RelayCommand(ExecuteDeleteTask);
            EndOfWorkCommand = new RelayCommand(ExecuteEndOfWork);
            DaySelectionChangedCommand = new RelayCommand(ExecuteDaySelectionChanged);
            LunchbreakCommand = new RelayCommand(ExecuteLunchbreak);
            OpenArchiveCommand = new RelayCommand(ExecuteOpenArchive);
            OpenInfoCommand = new RelayCommand(ExecuteOpenInfo);
            OpenSettingsCommand = new RelayCommand(ExecuteOpenSettings);
            OpenSummaryCommand = new RelayCommand(ExecuteOpenSummary);
            ReloadCommand = new RelayCommand(ExecuteReload);
            SelectionChangedCommand = new RelayCommand(ExecuteSelectionChanged);

            Items.CollectionChanged += Items_CollectionChanged;

            // Init
            ReloadDays();

            // Create today if not yet existing
            if (!Days.Any(x => x.Date == DateTime.Today))
            {
                CreateToday();
                ReloadDays();
            }

            SelectedDay = Days.First();

            ReloadItems();
            if (Items.Count > 0)
            {
                _parent.TextBoxProject.Text = Items.First().Project.Title;
                _parent.TextBoxProject.SelectAll();
            }

            // Refresh total worktime every minute
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Thread.Sleep(60000);
                    NotifyPropertyChanged(nameof(TotalWorkTimeString), false, nameof(CurrentRuntimeString));
                    ListItemHelper.AdjustDuration(Items);
                }
            });
        }

        private void AddEntry(DateTime dateTime, string projectTitle, string title)
        {
            using (var context = new DBContext())
            {
                var project = context.GetOrCreateProject(projectTitle);
                var task = context.GetOrCreateTask(title.Trim(), project);
                context.Entries.Add(new Entry { IDDay = SelectedDay.ID, StartTime = dateTime, IDTask = task.ID });
            }

            ReloadItems();
        }

        private bool ContainsEndOfTime(ObservableCollection<ListItemVm> items)
        {
            if (!items.Any())
                return false;

            return items[0].Task.Title == Resources.EndOfWork;
        }

        private void CreateToday()
        {
            using (var context = new DBContext())
            {
                var existing = context.Days.FirstOrDefault(x => x.Date == DateTime.Today);
                if (existing != null)
                {
                    // Revive existing item
                    existing.ArchivedDate = null;
                    existing.IsArchived = false;

                    context.Days.Update(existing);
                    return;
                }

                context.Days.Add(new Day() { Date = DateTime.Today });
            }
        }

        private DateTime? ParseAdjustInput(string s)
        {
            if (SelectedDay == null)
                return null;

            if (int.TryParse(s, out int minutesToSubstract))
                return SelectedDay.Date.Add(DateTime.Now.TimeOfDay).AddMinutes(-minutesToSubstract);

            if (DateTime.TryParseExact(s, "H:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime result))
                return SelectedDay.Date.Add(result.TimeOfDay);

            return null;
        }

        private void ReloadDays()
        {
            var selectedDay = SelectedDay;

            using (var context = new DBContext())
            {
                Days.ReplaceAll(context.Days.Where(x => !x.IsArchived).OrderByDescending(x => x.Date));
            }

            if (selectedDay != null)
                SelectedDay = Days.FirstOrDefault(x => x.Date == selectedDay.Date) ?? Days.FirstOrDefault();

            NotifyPropertyChanged(nameof(Days), false, nameof(SelectedDay));
        }

        private void ReloadItems()
        {
            Items.ReplaceAll(ListItemHelper.ReloadItems(SelectedDay));

            NotifyPropertyChanged(nameof(Items));
            ItemsChanged?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Removes the items entry from the list and also task and project, if they got no more child items.
        /// </summary>
        private void RemoveRecursive(ListItemVm item)
        {
            using (var context = new DBContext())
            {
                context.Entries.Remove(context.Entries.First(x => x.ID == item.Entry.ID));
                if (!context.Entries.Any(x => x.IDTask == item.Task.ID))
                {
                    context.Tasks.Remove(context.Tasks.First(x => x.ID == item.Task.ID));
                }

                if (!context.Tasks.Any(x => x.IDProject == item.Project.ID))
                {
                    context.Projects.Remove(context.Projects.First(x => x.ID == item.Project.ID));
                }
            }
        }

        /// <summary>
        /// Sets the background color for all rows that are directly before an identical row to show that you could delete it without harm.
        /// </summary>
        private void SetBackgroundColorForRedundantRows()
        {
            for (int i = 0; i < Items.Count - 1; i++)
            {
                if (Items[i] == Items[i + 1] // Identical
                    || (Items[i].Project == Items[i + 1].Project && Items[i].Task == Items[i + 1].Task) // Same project and task
                    || Items[i].Entry.StartTime == Items[i + 1].Entry.StartTime) // Same start time
                {
                    Items[i].BackgroundColor = _redundantBgColor;
                }
                else
                {
                    Items[i].BackgroundColor = Brushes.Transparent;
                }
            }
        }

        private DateTime? GetAdjustedDateTime()
        {
            if (string.IsNullOrWhiteSpace(_parent.TextBoxAdjust.Text))
            {
                _parent.TextBoxAdjust.Text = "0";
                return SelectedDay.Date + DateTime.Now.TimeOfDay;
            }

            var adjusted = ParseAdjustInput(_parent.TextBoxAdjust.Text);
            _parent.TextBoxAdjust.Text = "0";

            return adjusted;
        }

        #region Command Methods

        private void ExecuteAddTask()
        {
            if (string.IsNullOrWhiteSpace(_parent.TextBoxTask.Text))
                return;

            var parsedInput = GetAdjustedDateTime();
            if (parsedInput == null)
                return;

            AddEntry(parsedInput.Value, _parent.TextBoxProject.Text.Trim(), _parent.TextBoxTask.Text);

            _parent.TextBoxTask.Text = "";
            _parent.TextBoxTask.Focus();
        }

        /// <summary>
        /// Archives the selected day. It is no longer available over the main interface.
        /// </summary>
        /// <param name="obj"></param>
        private void ExecuteArchiveDay(object obj)
        {
            if (obj is Day item)
            {
                using (var context = new DBContext())
                {
                    var day = context.Days.First(x => x.Date == item.Date);
                    day.IsArchived = true;
                    day.ArchivedDate = DateTime.Now;
                    context.Days.Update(day);
                }

                ReloadDays();
                DaysChanged?.Invoke(this, EventArgs.Empty);

                _parent.ComboBoxDays.Focus();

                ReloadItems();
            }
        }

        private void ExecuteChangeLanguage()
        {
            switch (Language.Instance.ActiveLanguage)
            {
                case Languages.English:
                    Language.Instance.ActiveLanguage = Languages.Deutsch;
                    break;

                default:
                    Language.Instance.ActiveLanguage = Languages.English;
                    break;
            }

            ReloadDays();
        }

        private void ExecuteDaySelectionChanged()
        {
            ReloadItems();
        }

        private void ExecuteDeleteTask(object obj)
        {
            if (obj is ListItemVm item)
            {
                int index = Items.IndexOf(item);
                if (index >= 0)
                {
                    RemoveRecursive(item);

                    ReloadItems();
                    NotifyPropertyChanged(nameof(CurrentRuntime), false, nameof(CurrentRuntimeString));
                }
            }
        }

        private void ExecuteEndOfWork()
        {
            // Auswahl-Dialog
            var dialog = new InputDialog
            {
                Title = Resources.AddEndOfWork,
                Description = Resources.EnterEndOfWork,
                InputRegex = new Regex("[0-9:]"),
                TextRegex = new Regex("^[0-9]?[0-9](:[0-5][0-9])?$"),
                Input = DateTime.Now.ToString("HH:mm"),
            };

            if (dialog.ShowDialog() != true)
                return;

            bool success = DateTime.TryParse(dialog.Input, out DateTime time);
            success = int.TryParse(dialog.Input, out int hour) || success;

            if (!success)
                return;

            if (time == DateTime.MinValue)
                time = SelectedDay.Date.AddHours(hour);

            using (var context = new DBContext())
            {
                var task = context.GetOrCreateTask(Resources.EndOfWork, null);
                var eowEntry = context.Entries.FirstOrDefault(x => x.IDDay == SelectedDay.ID && x.IDTask == task.ID);

                if (eowEntry != null)
                {
                    eowEntry.StartTime = time;

                    context.Entries.Update(eowEntry);
                }
                else
                {
                    eowEntry = new Entry { IDDay = SelectedDay.ID, IDTask = task.ID, StartTime = time };
                    context.Entries.Add(eowEntry);
                }
            }

            ReloadItems();
        }

        private void ExecuteLunchbreak()
        {
            var time = GetAdjustedDateTime();
            if (time != null)
            {
                AddEntry(time.Value, "", Resources.Lunchbreak);
            }
            else if (SelectedDay.Date == DateTime.Today)
            {
                AddEntry(DateTime.Now, "", Resources.Lunchbreak);
            }
        }

        private void ExecuteOpenArchive()
        {
            var window = new ArchiveWindow { Owner = _parent, WindowStartupLocation = WindowStartupLocation.CenterOwner };
            window.Closed += ArchiveWindow_Closed;

            var vm = new ArchiveVm(window);
            window.DataContext = vm;

            vm.DayRestored += ArchiveVm_DayRestored;

            window.ShowDialog();
        }

        private void ExecuteOpenInfo()
        {
            new InfoWindow() { Owner = _parent, WindowStartupLocation = WindowStartupLocation.CenterOwner }.ShowDialog();
        }

        private void ExecuteOpenSettings()
        {
            var window = new SettingsWindow() { Owner = _parent, WindowStartupLocation = WindowStartupLocation.CenterOwner };
            window.ShowDialog();

            if (window.IsDirty)
            {
                ReloadDays();
                ReloadItems();
            }
        }

        private void ExecuteOpenSummary()
        {
            var window = new SummaryWindow() { Owner = _parent, WindowStartupLocation = WindowStartupLocation.CenterOwner };
            window.Closed += SummaryWindow_Closed;

            var vm = new SummaryVm(window, this);
            window.DataContext = vm;

            vm.DayEntryChanged += SummaryVm_DayEntryChanged;
            vm.DayArchived += SummaryVm_DayArchived;

            window.Show();
        }

        private void ExecuteReload()
        {
            ReloadItems();
        }

        private void ExecuteSelectionChanged(object obj)
        {
            if (obj is ListItemVm item)
            {
                _parent.TextBoxProject.Text = item.Project.Title;
                _parent.TextBoxTask.Text = item.Task.Title;
            }
        }

        #endregion Command Methods

        #region Handler

        private void ArchiveVm_DayRestored(object sender, EventArgs e)
        {
            ReloadDays();
            DaysChanged?.Invoke(this, EventArgs.Empty);
        }

        private void ArchiveWindow_Closed(object sender, EventArgs e)
        {
            if (sender is ArchiveWindow window && window.DataContext is ArchiveVm vm)
            {
                vm.DayRestored -= ArchiveVm_DayRestored;
            }
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            SetBackgroundColorForRedundantRows();
            NotifyPropertyChanged(nameof(Items), false, nameof(ReverseItems), nameof(TotalWorkTimeString), nameof(CurrentRuntimeString), nameof(RedFlagVisibility), nameof(GreenFlagVisibility));
        }

        private void SummaryVm_DayArchived(object sender, EventArgs e)
        {
            ReloadDays();
        }

        private void SummaryVm_DayEntryChanged(object sender, Day day)
        {
            if (SelectedDay.ID == day.ID) // Reload items if it's the currently selected day
                ReloadItems();
        }

        private void SummaryWindow_Closed(object sender, EventArgs e)
        {
            if (sender is SummaryWindow window && window.DataContext is SummaryVm vm)
            {
                vm.DayEntryChanged -= SummaryVm_DayEntryChanged;
                vm.DayArchived -= SummaryVm_DayArchived;
            }
        }

        #endregion Handler
    }
}