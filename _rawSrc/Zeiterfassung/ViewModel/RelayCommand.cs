﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace Zeiterfassung.ViewModel
{
    /// <summary>
    /// Offers commands that can have parameters and can check for execution conditions.
    /// </summary>
    public class RelayCommand : ICommand
    {
        private readonly Func<bool> _canExecute1;
        private readonly Predicate<object> _canExecute2ParameterNeeded;
        private readonly Action _execute1MethodWithoutParameterLess;
        private readonly Action<object> _execute2ParameterNeeded;

        /// <summary> Create an easy command, where CanExecute always returns True. </summary>
        /// <param name="methodWithoutParameter"> Defines the method to be called when the command is invoked. Parameterless function (neeeds no commandParameter). </param>
        public RelayCommand(Action methodWithoutParameter) : this(methodWithoutParameter, null) { }

        /// <summary> Create an easy command, where CanExecute always returns True. </summary>
        /// <param name="methodWithObjectParameter"> Defines the method to be called when the command is invoked. Needs commandParameter object. </param>
        public RelayCommand(Action<object> methodWithObjectParameter) : this(methodWithObjectParameter, null) { }

        /// <summary> Create command with simple Execute and CanExecute functions. </summary>
        /// <param name="methodWithoutParameter"> Defines the method to be called when the command is invoked. Parameterless method (neeeds no commandParameter). </param>
        /// <param name="canExecuteFunctionWithoutParamter"> Defines the method that determines whether the command can execute in its current state. Parameterless function (neeeds no commandParameter). </param>
        public RelayCommand(Action methodWithoutParameter, Func<bool> canExecuteFunctionWithoutParamter)
        {
            if (methodWithoutParameter == null)
                throw new ArgumentNullException(nameof(methodWithoutParameter));

            _execute1MethodWithoutParameterLess = methodWithoutParameter;
            _canExecute1 = canExecuteFunctionWithoutParamter;
        }

        /// <summary> Create command with Execute and CanExecute functions which needs commandParamters. </summary>
        /// <param name="methodWithObjectParameter"> Defines the method to be called when the command is invoked. Needs commandParameter object. </param>
        /// <param name="canExecuteFunctionWithObjectParameter"> Defines the method that determines whether the command can execute in its current state. Needs commandParameter object. </param>
        public RelayCommand(Action<object> methodWithObjectParameter, Predicate<object> canExecuteFunctionWithObjectParameter)
        {
            _execute2ParameterNeeded = methodWithObjectParameter ?? throw new ArgumentNullException(nameof(methodWithObjectParameter));
            _canExecute2ParameterNeeded = canExecuteFunctionWithObjectParameter;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary> Determines whether the command can execute in its current state. Using the function that is given by constructor or the default return-true function. </summary>
        /// <returns> True if this command can be executed; otherwise, false. </returns>
        [DebuggerStepThrough]
        public bool CanExecute()
        {
            return _canExecute1 == null || _canExecute1();
        }

        /// <summary> Function that determines whether the command can execute in its current state. </summary>
        /// <param name="parameter"> Data used by the command. If the command does not require data to be passed, this object can be set to null. </param>
        /// <returns> True if this command can be executed; otherwise, false. </returns>
        [DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return (_canExecute2ParameterNeeded == null) ? CanExecute() : _canExecute2ParameterNeeded(parameter);
        }

        /// <summary> Execute the command method that is given by constructor. </summary>
        public void Execute()
        {
            if (_execute1MethodWithoutParameterLess == null)
                _execute2ParameterNeeded(null);
            else
                _execute1MethodWithoutParameterLess();
        }

        /// <summary> Execute the command method that is given by constructor. </summary>
        /// <param name="parameter"> Data used by the command. If the command does not require data to be passed, this object can be set to null. </param>
        public void Execute(object parameter)
        {
            if (_execute2ParameterNeeded == null)
                _execute1MethodWithoutParameterLess();
            else
                _execute2ParameterNeeded(parameter);
        }

        #endregion ICommand Members

        /// <summary> Invalidates all (Relay)Commands. </summary>
        public static void RefreshAllCommands()
        {
            CommandManager.InvalidateRequerySuggested();
        }
    }
}