﻿using System;
using System.Reflection;
using Zeiterfassung.View;

namespace Zeiterfassung.ViewModel
{
    internal class InfoVm : ViewModelBase
    {
        private readonly InfoWindow _parent;

        public InfoVm(InfoWindow parent)
        {
            _parent = parent;
        }

        public string VersionString => "V" + Assembly.GetEntryAssembly().GetName().Version.ToString(3);
        public string AuthorString => "Gabriel Weidmann - " + DateTime.Now.Year;
    }
}