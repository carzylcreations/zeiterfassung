﻿using System;
using System.Windows.Media;
using Zeiterfassung.Data;

namespace Zeiterfassung.ViewModel
{
    public class ListItemVm : ViewModelBase, IComparable<ListItemVm>
    {
        private Brush _backgroundColor = Brushes.Transparent;
        private TimeSpan _duration;

        public Brush BackgroundColor
        {
            get { return _backgroundColor; }
            set
            {
                if (_backgroundColor != value)
                {
                    _backgroundColor = value;
                    NotifyPropertyChanged(nameof(BackgroundColor));
                }
            }
        }

        public TimeSpan Duration
        {
            get { return _duration; }
            set
            {
                _duration = value;
                NotifyPropertyChanged(nameof(Duration), false, nameof(DurationString));
            }
        }

        public string DurationString => ((Duration.TotalMinutes < 0) ? "-" : "") + Duration.ToString("hh':'mm");
        public Entry Entry { get; }
        public Project Project { get; }
        public string StartTimeString => Entry.StartTime.ToString("HH:mm");
        public Task Task { get; }

        /// <summary>
        /// Creates a new object.
        /// </summary>
        /// <param name="project"> The project. May be null. </param>
        /// <param name="task"> The task. May not be null. </param>
        /// <param name="entry"> The entry. May not be null. </param>
        public ListItemVm(Project project, Task task, Entry entry)
        {
            Project = project;
            Task = task ?? throw new ArgumentNullException(nameof(task));
            Entry = entry ?? throw new ArgumentNullException(nameof(entry));
        }

        public ListItemVm(ListItemVm item) : this(item.Project, item.Task, item.Entry)
        {
            Duration = item.Duration;
        }

        public static bool operator !=(ListItemVm a, ListItemVm b)
        {
            return !(a == b);
        }

        public static bool operator ==(ListItemVm a, ListItemVm b)
        {
            if (ReferenceEquals(a, null)) // Do not use "==" as this will lead to SOE
                return ReferenceEquals(b, null);

            return a.Equals(b);
        }

        /// <summary>
        /// Compares the project, task and starttime to another item.
        /// </summary>
        /// <returns> 0 if both are equal. A negative value if this item is smaller than the other one, otherwise a positive value. </returns>
        public int CompareTo(ListItemVm other)
        {
            int compare = Project.CompareTo(other.Project);
            if (compare != 0)
                return compare;

            compare = Task.CompareTo(other.Task);
            if (compare != 0)
                return compare;

            return Entry.StartTime.CompareTo(other.Entry.StartTime);
        }

        public override bool Equals(object obj)
        {
            if (obj is ListItemVm other)
                return CompareTo(other) == 0;

            return false;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;

                if (Project != null)
                    hash = (hash * 59) + Project.GetHashCode();
                if (Task != null)
                    hash = (hash * 59) + Task.GetHashCode();
                if (Entry != null)
                    hash = (hash * 59) + Entry.GetHashCode();

                return hash;
            }
        }

        public override string ToString()
        {
            return $"{Project?.Title} {Task?.Title} {DurationString}";
        }
    }
}