﻿using System;
using System.Linq;
using Zeiterfassung.Data;
using Zeiterfassung.Model.Custom;
using Zeiterfassung.Properties;
using Zeiterfassung.View;

namespace Zeiterfassung.ViewModel
{
	public class ArchiveVm : ViewModelBase
	{
		private readonly ArchiveWindow _window;

		public DateTime DateFrom { get; set; } = DateTime.Today.AddMonths(-1);
		public DateTime DateTo { get; set; } = DateTime.Today;
		public ObservableCollection<ArchiveItemVm> Items { get; } = new ObservableCollection<ArchiveItemVm>();

		public OrderByItem OrderBy { get; set; } = OrderByItem.DateDesc;

		public OrderByItem[] OrderByItems { get; } = OrderByItem.Items;

		public event EventHandler DayRestored;

		#region Commands

		public RelayCommand OrderByChangedCommand { get; }
		public RelayCommand SelectedDateChangedCommand { get; }

		#endregion Commands

		public ArchiveVm(ArchiveWindow window)
		{
			_window = window;

			OrderByChangedCommand = new RelayCommand(OnOrderByChanged);
			SelectedDateChangedCommand = new RelayCommand(OnSelectedDateChanged);

			ReloadItems();
		}

		private void ReloadItems()
		{
			using (var context = new DBContext())
			{
				var days = context.Days.Where(x => x.IsArchived && x.Date >= DateFrom && x.Date <= DateTo);

				if (OrderBy == OrderByItem.ArchivedAsc)
					days = days.OrderBy(x => x.ArchivedDate);
				else if (OrderBy == OrderByItem.ArchivedDesc)
					days = days.OrderByDescending(x => x.ArchivedDate);
				else if (OrderBy == OrderByItem.DateAsc)
					days = days.OrderBy(x => x.Date);
				else if (OrderBy == OrderByItem.DateDesc)
					days = days.OrderByDescending(x => x.Date);
				else
					throw new InvalidOperationException("Sort order not implemented.");

				Items.ReplaceAll(days.Select(x => new ArchiveItemVm(x) { Restored = Restored }));
			}
		}

		private void Restored(Day day)
		{
			DayRestored?.Invoke(this, EventArgs.Empty);
			ReloadItems();
		}

		#region Command Methods

		private void OnOrderByChanged()
		{
			ReloadItems();
		}

		private void OnSelectedDateChanged()
		{
			ReloadItems();
		}

		#endregion Command Methods

		public class OrderByItem
		{
			public static readonly OrderByItem ArchivedAsc = new OrderByItem("Archived", false);
			public static readonly OrderByItem ArchivedDesc = new OrderByItem("Archived", true);
			public static readonly OrderByItem DateAsc = new OrderByItem("Date", false);
			public static readonly OrderByItem DateDesc = new OrderByItem("Date", true);
			public static OrderByItem[] Items => new OrderByItem[] { DateAsc, DateDesc, ArchivedAsc, ArchivedDesc, };
			public bool Desc { get; }
			public string LanguageKey { get; }

			public string Title => $"{Resources.ResourceManager.GetString(LanguageKey, Resources.Culture)} ({(Desc ? Resources.Desc : Resources.Asc)})";

			public OrderByItem(string languageKey, bool desc)
			{
				LanguageKey = languageKey;
				Desc = desc;
			}

			public static bool operator !=(OrderByItem item1, OrderByItem item2)
			{
				return !(item1 == item2);
			}

			public static bool operator ==(OrderByItem item1, OrderByItem item2)
			{
				if (ReferenceEquals(item1, null))
				{
					return ReferenceEquals(item2, null);
				}

				return item1.Equals(item2);
			}

			public override bool Equals(object obj)
			{
				if (obj is OrderByItem item)
				{
					return LanguageKey == item.LanguageKey && Desc == item.Desc;
				}

				return false;
			}

			public override int GetHashCode()
			{
				unchecked // Overflow is fine, just wrap
				{
					int hash = 41;
					// Suitable nullity checks etc, of course :)

					hash = (hash * 59) + Desc.GetHashCode();

					if (LanguageKey != null)
						hash = (hash * 59) + LanguageKey.GetHashCode();

					return hash;
				}
			}
		}
	}
}