﻿using System.Diagnostics;
using System.IO;
using Zeiterfassung.Model;
using Zeiterfassung.Properties;
using Zeiterfassung.View;
using Ookii.Dialogs.Wpf;

namespace Zeiterfassung.ViewModel
{
    internal class SettingsVm : ViewModelBase
    {
        private readonly SettingsWindow _parent;
        private string _path;

        #region Commands

        public RelayCommand ChangePathCommand { get; }
        public RelayCommand OpenPathCommand { get; }
        public RelayCommand ResetPathCommand { get; }

        #endregion Commands

        /// <summary>
        /// Were some changes made that require a reload of the main windows items?
        /// </summary>
        public bool IsDirty { get; private set; }

        /// <summary>
        /// The path where the backups are stored.
        /// </summary>
        public string Path
        {
            get => _path;
            private set
            {
                _path = value;
                IsDirty = true;
            }
        }

        public SettingsVm(SettingsWindow parent)
        {
            _parent = parent;

            ChangePathCommand = new RelayCommand(ExecuteChangePath);
            ResetPathCommand = new RelayCommand(ExecuteResetPath);
            OpenPathCommand = new RelayCommand(ExecuteOpenPath);

            Path = ValidatePath(Settings.Default.BackupFolderPath);
        }

        private void ExecuteChangePath()
        {
            //using (var dialog = new FolderBrowserDialog() { Description = Resources.ChooseSavingFolder, ShowNewFolderButton = true })
            var dialog = new VistaFolderBrowserDialog() { Description = Resources.ChooseSavingFolder, ShowNewFolderButton = true, UseDescriptionForTitle = true };

            if (Directory.Exists(Settings.Default.BackupFolderPath))
                dialog.SelectedPath = Settings.Default.BackupFolderPath;

            if (dialog.ShowDialog() == true)
            {
                SetPath(dialog.SelectedPath);
            }

        }

        private void ExecuteOpenPath()
        {
            string path = Path;
            if (string.IsNullOrWhiteSpace(Path))
                path = StaticValues.DefaultBackupFolderPath;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            Process.Start("explorer.exe", path);
        }

        private void ExecuteResetPath()
        {
            Settings.Default.BackupFolderPath = "";
            Settings.Default.Save();

            Path = "";
            NotifyPropertyChanged(nameof(Path));
        }

        private void SetPath(string path)
        {
            Settings.Default.BackupFolderPath = path;
            Settings.Default.Save();

            Path = path;
            NotifyPropertyChanged(nameof(Path));
        }

        private string ValidatePath(string backupFolderPath)
        {
            if (Directory.Exists(backupFolderPath))
                return backupFolderPath;
            else
                return "";
        }
    }
}