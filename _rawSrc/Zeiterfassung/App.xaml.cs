﻿using System.Collections.Generic;
using System.Windows;
using Zeiterfassung.Controller;
using Zeiterfassung.Model.Lang;

namespace Zeiterfassung
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        public bool SignalExternalCommandLineArgs(IList<string> args)
        {
            // handle command line arguments of second instance
            // …

            return true;
        }
    }
}